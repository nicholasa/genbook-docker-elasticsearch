#Genbook Dev ElasticSearch Docker

From https://hub.docker.com/_/elasticsearch/

Current ElasticSearch version: 2.3.2

Installs plugins:
```
mobz/elasticsearch-head
royrusso/elasticsearch-HQ
```

##Building
`docker build -t "gb:elasticsearch" .`

##Running
`docker run -d -p 9200:9200 -p 9300:9300 -v "$PWD/esdata":/usr/share/elasticsearch/data --name gb-elasticsearch gb:elasticsearch -Des.cluster.name=CHANGEME`
